import os
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls.base import reverse_lazy
from django.utils.safestring import mark_safe
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from markdown import markdown

from projects.forms import ProjectForm
from projects.models import Project, Tag

from projects.utils import (
                    ObjectDetailMixin,
                    ObjectCreateMixin,
                    ObjectUpdateMixin,
                    ObjectDeleteMixin,
                )

from projects.forms import TagForm


class AssignmentView(TemplateView):
    template_name = 'projects/assignment.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        with open(os.path.join(os.path.dirname(settings.BASE_DIR), 'README.md'), encoding='utf-8') as f:
            assignment_content = f.read()

        context.update({
            'assignment_content': mark_safe(markdown(assignment_content))
        })

        return context


class DashboardView(LoginRequiredMixin, ListView):
    model = Project
    ordering = ('-end_date',)
    context_object_name = 'projects'
    template_name = 'projects/dashboard.html'

    def get_queryset(self):
        projects = super().get_queryset()
        projects = projects.select_related('company')
        new_list_projects = []
        for project in projects:
            if project.end_date is None:
                new_list_projects.insert(0, project)
            else:
                new_list_projects.append(project)
        return new_list_projects


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    success_url = reverse_lazy('dashboard')


class TagDetail(ObjectDetailMixin, View):
    model = Tag
    template = 'projects/tag_detail.html'


class TagDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Tag
    redirect_url = 'tags_list_url'
    template = 'projects/tag_delete_form.html'

    raise_exception = True


class TagCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    form_model = TagForm
    template = 'projects/tag_create.html'

    raise_exceptions = True


class TagUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Tag
    form_model = TagForm
    template = 'projects/tag_update_form.html'

    raise_exceptions = True


def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'projects/tags_list.html', context={'tags': tags})



