from projects.models import Project
from celery import app
@app.task
def do_stuff(self, pk):
    my_data = Project.objects.get(pk=pk)
    my_data.save()