from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from time import time


def gen_slug(s):
    new_slug = slugify(s, allow_unicode=True)
    return new_slug + '-' + str(int(time()))


class Company(models.Model):

    class Meta:
        verbose_name_plural = "companies"

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Project(models.Model):
    company = models.ForeignKey('projects.Company', on_delete=models.PROTECT, related_name='projects')
    title = models.CharField('Project title', max_length=128)
    start_date = models.DateField('Project start date', blank=True, null=True)
    end_date = models.DateField('Project end date', blank=True, null=True)
    version = models.IntegerField(
        default=0
    )
    estimated_design = models.DecimalField(max_digits=7, decimal_places=2)
    actual_design = models.DecimalField(max_digits=7, decimal_places=2)

    estimated_development = models.DecimalField(max_digits=7, decimal_places=2)
    actual_development = models.DecimalField(max_digits=7, decimal_places=2)

    estimated_testing = models.DecimalField(max_digits=7, decimal_places=2)
    actual_testing = models.DecimalField(max_digits=7, decimal_places=2)

    tags = models.ManyToManyField('Tag', blank=True, related_name='projects')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('project-update', kwargs={'pk': self.pk, 'slug': slugify(self.title)})

    @property
    def has_ended(self):
        return self.end_date is not None and self.end_date < timezone.now().date()

    @property
    def total_estimated_hours(self):
        return self.estimated_design + self.estimated_development + self.estimated_testing

    @property
    def total_actual_hours(self):
        return self.actual_design + self.actual_development + self.actual_testing

    @property
    def is_over_budget(self):
        return self.total_actual_hours > self.total_estimated_hours


class Tag(models.Model):
    title = models.CharField(max_length=16)
    slug = models.SlugField(max_length=16, unique=True)

    def get_absolute_url(self):
        return reverse('tag_detail_url', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('tag_update_url', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('tag_delete_url', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = gen_slug(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']