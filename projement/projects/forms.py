from django.core.exceptions import ValidationError
from django.forms.models import ModelForm
from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from projects.models import Project, Tag


class ProjectForm(ModelForm):

    class Meta:
        model = Project
        fields = ['actual_design', 'actual_development', 'actual_testing']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'UPDATE'))


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['title', 'slug']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()

        if new_slug == 'create':
            raise ValidationError('Slug may not be create')
        elif Tag.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError('Slug must be unique. We have "{}" slug already'.format(new_slug))
        return new_slug